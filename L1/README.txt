Name: Duc Minh Le
DrexelID: dml352
Lab1 - Fibonacci page

I completed this lab alone.
The URL for this page: https://www.cs.drexel.edu/~dml352/CS275/webApp1/dml352_lab1.html
The page is used to calculate Fibonacci number up to a given position. The picture was taken from the 
Wikipedia page about Fibonacci sequence (https://en.wikipedia.org/wiki/Fibonacci_number). There is a text box,
where user can input a non-negative number. If user enters a negative number, an error message will be printed.
If user enters a non-negative number, the page will created a table of Fibonacci number up to that position.
2 functions is used in this page. fib recursively calculate the Fibonacci number at a given position.
compute2 will create our table, or create the error message.
Name: Duc Minh Le
Drexel Id: dml352

I comleted this lab alone. 
The URL: https://www.cs.drexel.edu/~dml352/CS275/TicTacToe.html
The board is rendered as a 3x3 table. The underlying structure is an array of integers: 0 for empty, 1 for X, 2 for O.
Whenever a cell is clicked, we modify the array and re-render the board accordingly. The game continues until we have
a winner, or it becomes a tie. 
There are alerts for clicking on non-empty squares. Also, the game can always be reset using the reset button.

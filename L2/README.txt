Name: Duc Le
DrexelId: dml352

I completed this lab alone.
The webpage takes your API key. Then, the data for the key is returned. Next, another Ajax call is executed, 
using the zip code to get hourly_forecast data. Then, the table is created using the data from the returned
JSON, and is displayed to the screen.
lab2.html is the source code. I cannot put it on my directory on tux. When the page load on tux (https://www.cs.drexel.edu/~dml352/CS275/lab2.html),
it cannot execute ajax call, because drexel.edu is loaded through https, and the api call is considered insecure.
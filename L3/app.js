var express = require('express'); 
var app = express(); 
var mysql = require('mysql');
var con = mysql.createConnection({
	host: 'twilbury.cs.drexel.edu',
	user: 'dml352',
	password: 'flwyyatoy',
	database: 'dml352_cs275-001_201504'
});
var error;
con.connect(function(err) {
	if (err)
		error = "Error connecting";
	else
		error = "Connected";
});
console.log(error);

app.get("/b", function(req, res) {
	con.query('select id, firstName, lastName from Students order by id asc',
		function(err, rows, fields) {
			if (err)
				res.send("Error querying");
			else
				res.json(rows);
		});
});

app.get("/a1", function(req, res) {
	con.query('select * from Students',
		function(err, rows, fields) {
			if (err)
				res.send("Error querying");
			else
				res.json(rows);
		});
});

app.get("/c", function(req, res) {
	con.query("select distinct courseName from CourseGrades where term='Fall'",
		function(err, rows, fields) {
			if (err)
				res.send("Error querying");
			else
				res.json(rows);
		});
});

app.get("/a2", function(req, res) {
	con.query('select * from CourseGrades',
		function(err, rows, fields) {
			if (err)
				res.send("Error querying");
			else
				res.json(rows);
		});
});

app.get("/d", function(req, res) {
	con.query('select id, courseName, term from CourseGrades order by term',
		function(err, rows, fields) {
			if (err)
				res.send("Error querying");
			else
				res.json(rows);
		});
});

app.get("/e", function(req, res) {
	con.query('select s.firstName, s.lastName, s.major, g.courseName, g.term, g.grade from Students s inner join CourseGrades g on s.id = g.id',
		function(err, rows, fields) {
			if (err)
				res.send("Error querying");
			else
				res.json(rows);
		});
});

app.get("/f", function(req, res) {
	con.query('select count(distinct id) from Students',
		function(err, rows, fields) {
			if (err)
				res.send("Error querying");
			else
				res.json(rows);
		});
});

app.get("/g", function(req, res) {
	con.query('select distinct courseName from CourseGrades',
		function(err, rows, fields) {
			if (err)
				res.send("Error querying");
			else
				res.json(rows);
		});
});

app.get("/h", function(req, res) {
	con.query('select s.id, s.firstName, s.lastName, g.courseName, g.grade from Students s inner join CourseGrades g on s.id = g.id',
		function(err, rows, fields) {
			if (err)
				res.send("Error querying");
			else
				res.json(rows);
		});
});

app.get("/i", function(req, res) {
	con.query("select s.id, s.firstName, s.lastName, g.courseName, g.grade from Students s inner join CourseGrades g on s.id = g.id where g.courseName = 'CS275'",
		function(err, rows, fields) {
			if (err)
				res.send("Error querying");
			else
				res.json(rows);
		});
});

app.get("/j", function(req, res) {
	con.query('select distinct major from Students',
		function(err, rows, fields) {
			if (err)
				res.send("Error querying");
			else
				res.json(rows);
		});
});

app.listen(3000);
